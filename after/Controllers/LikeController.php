<?php

namespace SOLID\After\InterfaceSegregation\Controllers;

class LikeController
{
    public function like(LikeableInterface $likeable, LikerInterface $liker)
    {
        $liker->like($likeable);
    }

    public function dislike(LikeableInterface $likeable, LikerInterface $liker)
    {
        if (! $liker->hasLiked($likeable)) {
            throw new InvalidActionException("Cannot dislike a non liked Post");
        }

        $liker->dislike($likeable);
    }
}
