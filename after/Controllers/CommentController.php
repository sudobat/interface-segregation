<?php

namespace SOLID\After\InterfaceSegregation\Controllers;

class CommentController
{
    public function comment(CommentableInterface $commentable, CommenterInterface $commenter, CommentInterface $comment)
    {
        $commenter->comment($commentable, $comment);
    }
}
