<?php

namespace SOLID\After\InterfaceSegregation\Models;

class User implements Commenter, Liker
{
    private $firstName;
    private $lastName;

    private $age;
    private $gender;

    private $description;

    private $createdAt;
    private $updatedAt;

    private $posts;

    private $comments;

    public function comment(CommentableInterface $commentable, CommentInterface $comment)
    {
        $commentable->addComment($comment);
    }

    public function like(LikeableInterface $likeable)
    {
        $likeable->addLike();
    }

    public function dislike(LikeableInterface $likeable)
    {
        $likeable->removeLike();
    }
}
