<?php

namespace SOLID\After\InterfaceSegregation\Models;

class Post implements Commentable, Likeable
{
    private $title;
    private $description;

    private $createdAt;

    private $comments;

    private $author;

    private $likes;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getAuthor(): AuthorInterface
    {
        return $this->author;
    }

    public function addAuthor(AuthorInterface $author)
    {
        $this->author = $author;
    }

    public function addComment(CommentInterface $comment)
    {
        $this->comments[] = $comment;
    }

    public function addLike()
    {
        $this->likes++;
    }

    public function removeLike()
    {
        if ($this->likes <= 0) {
            return;
        }

        $this->likes--;
    }
}
