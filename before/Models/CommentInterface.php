<?php

namespace SOLID\Before\InterfaceSegregation\Models;

interface CommentInterface
{
    public function getContent(): string;

    public function getCreatedAt(): \DateTime;

    public function getAuthor(): UserInterface;
}
