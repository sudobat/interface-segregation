<?php

namespace SOLID\Before\InterfaceSegregation\Models;

interface PostInterface
{
    public function getTitle(): string;

    public function getDescription(): string;

    public function getCreatedAt(): \DateTime;

    public function getAuthor(): UserInterface;

    public function addAuthor(UserInterface $user);

    public function addComment(CommentInterface $comment);

    public function addLike();

    public function removeLike();
}
