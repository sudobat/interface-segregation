<?php

namespace SOLID\Before\InterfaceSegregation\Models;

class Post implements PostInterface
{
    private $title;
    private $description;

    private $createdAt;

    private $comments;

    private $author;

    private $likes;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getAuthor(): UserInterface
    {
        return $this->author;
    }

    public function addAuthor(UserInterface $user)
    {
        $this->author = $user;
    }

    public function addComment(CommentInterface $comment)
    {
        $this->comments[] = $comment;
    }

    public function addLike()
    {
        $this->likes++;
    }

    public function removeLike()
    {
        if ($this->likes <= 0) {
            return;
        }

        $this->likes--;
    }
}
