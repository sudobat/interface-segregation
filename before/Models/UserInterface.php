<?php

namespace SOLID\Before\InterfaceSegregation\Models;

interface UserInterface
{
    public function getFullName(): string;

    public function comment(PostInterface $post, CommentInterface $comment);

    public function like(PostInterface $post);

    public function dislike(PostInterface $post);
}
