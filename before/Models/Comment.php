<?php

namespace SOLID\Before\InterfaceSegregation\Models;

class Comment implements CommentInterface
{
    private $content;

    private $createdAt;

    private $post;

    private $author;

    public function getContent(): string
    {
        return $this->content;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getAuthor(): UserInterface
    {
        return $this->author;
    }
}
