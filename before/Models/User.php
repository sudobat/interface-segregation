<?php

namespace SOLID\Before\InterfaceSegregation\Models;

class User implements UserInterface
{
    private $firstName;
    private $lastName;

    private $age;
    private $gender;

    private $description;

    private $createdAt;
    private $updatedAt;

    private $posts;

    private $comments;

    public function getFullName(): string
    {
        return $this->firstName." ".$this->lastName;
    }

    public function comment(PostInterface $post, CommentInterface $comment)
    {
        $post->addComment($comment);
    }

    public function like(PostInterface $post)
    {
        $post->addLike();
    }

    public function dislike(PostInterface $post)
    {
        $post->removeLike();
    }
}
