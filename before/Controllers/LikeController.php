<?php

namespace SOLID\Before\InterfaceSegregation\Controllers;

class LikeController
{
    public function like(PostInterface $post, UserInterface $user)
    {
        $user->like($post);
    }

    public function dislike(PostInterface $post, UserInterface $user)
    {
        if (! $user->hasLiked($post)) {
            throw new InvalidActionException("Cannot dislike a non liked Post");
        }

        $user->dislike($post);
    }
}
