<?php

namespace SOLID\Before\InterfaceSegregation\Controllers;

class CommentController
{
    public function comment(PostInterface $post, UserInterface $user, CommentInterface $comment)
    {
        $user->comment($post, $comment);
    }
}
