<?php

namespace SOLID\Before\InterfaceSegregation\Controllers;

class PostController
{
    public function show(PostInterface $post)
    {
        return view('post.single')->with([
            'title'             => $post->getTitle(),
            'description'       => $post->getDescription(),
            'createdAt'         => $post->getCreatedAt(),
            'comments'          => $post->getComments(),
            'authorFullName'    => $post->getAuthor()->getFullName()
        ]);
    }

    public function addAuthor(PostInterface $post, UserInterface $user)
    {
        $post->addAuthor($user);
    }
}
